from pathlib import Path
import os
import re
import pymysql.cursors
import subprocess
import shutil
import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import inflect

def take_dump_from_database(username,password,database,file_path,deleted_old_files):
    try:
        # if deleted_old_files == False:
        #     dir = os.listdir(os.getcwd()+"\\DB_FILE")
        #     for file in dir:
        #         os.remove(os.getcwd()+"\\DB_FILE\\" + file)
        with open(file_path, 'w') as output:
            c = subprocess.Popen(['C:\Program Files (x86)\MySQL\MySQL Server 5.1\\bin\\mysqldump.exe',
                                  '-u', username, '-p%s' % password, database, '--extended-insert=FALSE', "--complete-insert"],
                                  stdout=output, shell=True)
    except Exception as e:
        print(e)
        send_mail_if_restore_was_failed(e)

def compare_structure(table_length_of_files,files):
    try:
        if table_length_of_files:
            file_one = files[0]
            file_two = files[1]
            for total_length in range(len(file_one)):
                table_one = file_one[total_length]
                table_two = file_two[total_length]
                split_table_one = table_one.split(") ENGINE=")[0]
                split_table_two = table_two.split(") ENGINE=")[0]
                splited_table_name = split_table_one.split(",")
                table_nm = splited_table_name[0].split("CREATE TABLE `")[1].split("`")[0]
                table_list.append(table_nm)
                if split_table_one != split_table_two:
                    # THIS LOGIC IS USE TO ADD COLUMN IN PROD ENV DATA BASE
                    splited_table = split_table_one.split('CREATE TABLE `' + table_nm + '`' + ' (')[1].split('PRIMARY KEY (`')
                    splited_table_two = split_table_two.split('CREATE TABLE `' + table_nm + '`' + ' (')[1].split('PRIMARY KEY (`')
                    splited_table = splited_table[0].split(',')
                    splited_table_two = splited_table_two[0].split(',')
                    if len(splited_table) > len(splited_table_two):
                        for i in range(len(splited_table)):
                            if splited_table[i] not in splited_table_two:
                                new_column_name = splited_table[i]
                                table_name = table_nm
                                after_column_name = splited_table[i - 1].split('`')[1].split("`")[0]
                                sql = "ALTER table " + "`" + table_name + "`" + " add column " + new_column_name + " AFTER " + "`" + after_column_name + "`"
                                sql = sql.replace(' \n', '')
                                # if sql.endswith(')') or sql.endswith(');') or sql.endswith(') ;'):
                                #     sql = sql
                                # else:
                                #     sql += ')'
                                with connection.cursor() as cursor:
                                    cursor.execute(sql)
                                    connection.commit()
                                print("split_table_one != split_table_two==>", sql)
                    # THIS LOGIC IS USE TO DROP COLUMN IN PROD ENV DATA BASE
                    if len(splited_table) < len(splited_table_two):
                        for i in range(len(splited_table_two)):
                            if splited_table_two[i] not in splited_table:
                                # print(splited_table[i])
                                table_name = table_nm
                                drop_column_name = splited_table_two[i].split('`')[1].split("`")[0]
                                sql = "ALTER TABLE " + "`" + table_name + "`" + " DROP COLUMN " + "`" + drop_column_name + "`"
                                sql = sql.replace(' \n', '')
                                # if sql.endswith(')') or sql.endswith(');') or sql.endswith(') ;'):
                                #     sql = sql
                                # else:
                                #     sql += ')'
                                with connection.cursor() as cursor:
                                    cursor.execute(sql)
                                    connection.commit()
                                print("len(splited_table) < len(splited_table_two)==>", sql)
                    # THIS LOGIC IS USE TO CHANGE COLUMN IN PROD ENV DATA BASE
                    if len(splited_table) == len(splited_table_two):
                        for i in range(len(splited_table)):
                            if splited_table[i] != splited_table_two[i]:
                                table_name = table_nm
                                column_name = splited_table_two[i].split('`')[1].split("`")[0]
                                sql = "ALTER TABLE " + "`" + table_name + "`" + " CHANGE COLUMN " + "`" + column_name + "`" + ' ' + \
                                      splited_table[i]
                                sql = sql.replace(' \n', '')
                                # if sql.endswith(')') or sql.endswith(');') or sql.endswith(') ;'):
                                #     sql = sql
                                # else:
                                #     sql += ')'
                                with connection.cursor() as cursor:
                                     cursor.execute(sql)
                                     connection.commit()
                                print("len(splited_table) == len(splited_table_two)", sql)
                            # if splited_table[i] not in splited_table_two:
                            #     new_column_name = splited_table[i]
                            #     table_name = splited_table[0].split("CREATE TABLE `")[1].split("`")[0]
                            #     after_column_name = splited_table[i - 1].split('`')[1].split("`")[0]
                            #     sql = "ALTER table " + "`" + table_name + "`" + " add column " + new_column_name + " AFTER " + "`" + after_column_name + "`"
                            #     sql = sql.replace(' \n', '')
                            #     with connection.cursor() as cursor:
                            #         cursor.execute(sql)
                            #         connection.commit()
                            #     print("upper_sql==>", sql)
                            # if splited_table_two[i] not in splited_table:
                            #     new_column_name = splited_table_two[i]
                            #     table_name = splited_table_two[0].split("CREATE TABLE `")[1].split("`")[0]
                            #     drop_column_name = splited_table_two[i].split('`')[1].split("`")[0]
                            #     sql = "ALTER TABLE " + "`" + table_name + "`" + " DROP COLUMN " + "`" + drop_column_name + "`"
                            #     sql = sql.replace(' \n', '')
                            #     with connection.cursor() as cursor:
                            #         cursor.execute(sql)
                            #         connection.commit()
                            #     print("upper_sql==>", sql)
    except Exception as e:
        print("IN STRUCTURE COMPARE==>",e)
        revert_back_database(schema_name=qa_database, username=qa_user, password=qa_password, file_path=qa_file_path)

def create_table(table_length_of_files,files):
    try:
        if table_length_of_files is False:
            file_one = files[0]
            file_two = files[1]
            table_nm_dev = []
            table_nm_prod = []
            for total_length in range(len(file_one)):
                table_one = file_one[total_length]
                split_table_one = table_one.split(" ENGINE=")[0]
                splited_table_name = split_table_one.split(",")
                table_nm = splited_table_name[0].split("CREATE TABLE `")[1].split("`")[0]
                table_nm_dev.append(table_nm)
            for total_length in range(len(file_two)):
                table_one = file_two[total_length]
                split_table_one = table_one.split(" ENGINE=")[0]
                splited_table_name = split_table_one.split(",")
                table_nm = splited_table_name[0].split("CREATE TABLE `")[1].split("`")[0]
                table_nm_prod.append(table_nm)

            if len(table_nm_prod) > len(table_nm_dev):
                for i in range(len(table_nm_prod)):
                    if table_nm_prod[i] not in table_nm_dev:
                        sql = "DROP TABLE " +table_nm_prod[i]
                        print("len(table_nm_prod) > len(table_nm_dev)==>",sql)
                        if sql.endswith(')') or sql.endswith(');') or sql.endswith(') ;'):
                            sql = sql
                        else:
                            sql += ')'
                        with connection.cursor() as cursor:
                              cursor.execute(sql)
                              connection.commit()
            if len(table_nm_prod) < len(table_nm_dev):
                fk_list = []
                cr_tables = []
                lst_query = []
                for i in range(len(table_nm_dev)):
                    if table_nm_dev[i] not in table_nm_prod:
                        tables = []
                        query_list = []
                        print(table_nm_dev[i])
                        f = open(FOLDER_PATH + dev_file, "r", encoding="utf-8")
                        lines = f.readlines()
                        query_endofcreate = False
                        query_statement = ""
                        queries = []
                        table_endofcreate = False
                        table_statement = ""
                        create_table = []
                        for line in lines:
                            if line.__contains__('INSERT INTO `' + table_nm_dev[i]) or query_endofcreate:
                                query_endofcreate = True
                                query_statement = query_statement + " " + line
                            if line.__contains__('/*!40000 ALTER TABLE `' + table_nm_dev[i] + '`' + ' ENABLE KEYS '):
                                query = re.sub(
                                    "[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]",
                                    'NULL', query_statement).replace("\'NULL\'", 'NULL')
                                query = query.replace('/'
                                                      '*!40000 ALTER TABLE ' + '`' + table_nm_dev[i] + '` ' + 'ENABLE KEYS */;', '')
                                queries.append(query)
                                query_endofcreate = False
                            if line.__contains__('CREATE TABLE `' + table_nm_dev[i]) or table_endofcreate:
                                table_endofcreate = True
                                table_statement = table_statement + " " + line
                            if line.__contains__('ENGINE=') and table_endofcreate:
                                create_table.append(table_statement)
                                table_statement = ""
                                table_endofcreate = False
                        query_list.append(queries)
                        lst_query.append(query_list)
                        tables.append(create_table)
                        for i in range(len(tables)):
                            stri = ""
                            stri_two = ""
                            t_tables = str(tables[i]).split(',')
                            for j in range(len(t_tables)):
                                if 'KEY `' not in t_tables[j] and 'CONSTRAINT `' not in t_tables[j]:
                                    stri =  stri + " " +  t_tables[j] + ","
                                    stri = stri.replace('[','').replace('\'','').replace(']','')
                                else:
                                    stri_two = stri_two + " " + t_tables[j] + ","
                                    stri_two = stri_two.replace('[', '').replace('\'', '').replace(']', '')
                            cr_tables.append(stri.replace('\\n','').replace('   ','')[:-1] + ');')
                            tbl_name = stri.split('CREATE TABLE `')[1].split('`')[0]
                            fk_list.append(stri_two.replace('\\n','').replace('   ','') + '*-*-*' + tbl_name)
                for i in range(len(cr_tables)):
                    if " ENGINE=" in cr_tables[i]:
                        sql = cr_tables[1].split(' ENGINE=')
                        sql = sql[0] + ';'
                    else:
                        sql = cr_tables[i]
                    print("CREATE TABLE==>", sql)
                    if sql.endswith(')') or sql.endswith(');') or sql.endswith(') ;'):
                        sql = sql
                    else:
                        sql += ')'
                    with connection.cursor() as cursor:
                        cursor.execute(sql)
                        connection.commit()
                for i in range(len(lst_query)):
                    qry = str(lst_query[i]).split('INSERT')
                    for j in range(len(qry)):
                        q = qry[j].split(');')
                        if 'INTO' in q[0]:
                           sql = "INSERT " + qry[j].replace('\\n','')
                           if ');' not in sql:
                               sql += ');'
                           if "); \"]]" in sql:
                               sql = sql[:-4]
                           print("for i in range(len(lst_query))==>",sql)
                           if sql.endswith(')') or sql.endswith(');') or sql.endswith(') ;'):
                               sql = sql
                           else:
                               sql += ')'
                           with connection.cursor() as cursor:
                               cursor.execute(sql)
                               connection.commit()
                for i in range(len(fk_list)):
                    tbl_name = fk_list[i].split('*-*-*')[1]
                    constraint = fk_list[i].split('CONSTRAINT')
                    for j in range(len(constraint)):
                        if 'FOREIGN KEY' in constraint[j]:
                            sql = constraint[j].split('CASCADE )')
                            sql = 'ALTER TABLE ' + tbl_name +' ADD CONSTRAINT ' + sql[0] + ' CASCADE ;'
                            print("j in range(len(constraint))==>",sql)
                            if sql.endswith(')') or sql.endswith(');') or sql.endswith(') ;'):
                                sql = sql
                            else:
                                sql += ')'
                            with connection.cursor() as cursor:
                                cursor.execute(sql)
                                connection.commit()
    except Exception as e:
        print("DROP OR CREATE TABLE==>",e)
        revert_back_database(schema_name=qa_database, username=qa_user, password=qa_password, file_path=qa_file_path)



def query_data_compare():

    try:
        non_data_list = ['agent_details', 'agent_service_queue', 'client_info', 'client_specific_software_management',
                         'company_db_connection',
                         'customer_stripe_invoice', 'df_agents', 'email_configuration', 'humanagent_user_map',
                         'mdm_user_details', 'payment_portal', 'scheduler_details',
                         'scheduler_subservice_type_company_map', 'service', 'smart_bot_handler', 'stripe_customers',
                         'stripe_invoice', 'stripe_subscriptions_customer',
                         'user_chat_transfer', 'user_details', 'user_module_map', 'visitor_info']
        data_list = table_list
        for i in non_data_list:
            if i in data_list:
                data_list.remove(i)
        for table in data_list:
            queries_list = []
            list_of_dev_id = []
            list_of_prod_id = []
            dev_queries = []
            prod_queries = []
            for j in txt_files:
                f = open(FOLDER_PATH + j,"r",encoding="utf-8")
                lines = f.readlines()
                query_endofcreate = False
                query_statement = ""
                queries = []
                for line in lines:
                    if line.__contains__('INSERT INTO `' + table) or query_endofcreate:
                        query_endofcreate = True
                        query_statement = query_statement+" "+line
                    if line.__contains__('/*!40000 ALTER TABLE `' +table + '`' + ' ENABLE KEYS '):
                        query = re.sub("[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]",
                               'NULL', query_statement).replace("\'NULL\'", 'NULL')
                        query = query.replace('/'
                                              '*!40000 ALTER TABLE ' +'`'+ table + '` ' +'ENABLE KEYS */;','')
                        queries.append(query)
                        query_endofcreate = False
                queries_list.append(queries)
            dev_query = queries_list[0]
            prod_query = queries_list[1]
            dev = dev_query[0].split(');')
            prod = prod_query[0].split(');')
            for i in dev:
                i = i.strip()
                dev_queries.append(i)
            for i in prod:
                i = i.strip()
                prod_queries.append(i)
            dev_queries.pop()
            prod_queries.pop()
            for i in range(len(dev_queries)):
                values = dev_queries[i].split("VALUES (")
                id = values[1].split(',')
                list_of_dev_id.append(id[0])
            for i in range(len(prod_queries)):
                values = prod_queries[i].split("VALUES (")
                id = values[1].split(',')
                list_of_prod_id.append(id[0])
            if len(dev_queries) > len(prod_queries):
                for i in dev_queries:
                    values = i.split("VALUES (")
                    id = values[1].split(',')
                    if id[0] not in list_of_prod_id:
                        sql = i
                        if sql.endswith(')') or sql.endswith(');') or sql.endswith(') ;') :
                            sql = sql
                        else:
                            sql += ')'
                        with connection.cursor() as cursor:
                            cursor.execute(sql)
                            connection.commit()
                    else:
                        # "id is in prod"
                        for j in range(len(list_of_prod_id)):
                            if list_of_prod_id[j] == id[0]:
                                data = prod_queries[j].split("VALUES")
                                data = data[0].split("INSERT INTO " + '`' + table + '`' + ' (')[1].replace('`', '').replace(')','').split(',')
                                stri = ""
                                for d in data:
                                    d = d.strip()
                                    stri += '`' + d + '`' + '=values(`' + d + '`),'
                                stri = stri[:-1]
                                sql = prod_queries[j] + ") ON DUPLICATE KEY UPDATE " + stri
                                print("UPDATE==>", sql)
                                if sql.endswith(')') or sql.endswith(');') or sql.endswith(') ;'):
                                    sql = sql
                                else:
                                    sql += ')'
                                with connection.cursor() as cursor:
                                    cursor.execute(sql)
                                    connection.commit()
            if len(dev_queries) < len(prod_queries):
                for i in prod_queries:
                    if i not in dev_queries:
                        values = i.split("VALUES (")
                        id = values[1].split(',')
                        name = values[0].split('(`')
                        column_name = name[1].split('`,')
                        if id[0] not in list_of_dev_id:
                            sql = "DELETE from " + table + " where " + column_name[0] + ' = ' + id[0]
                            print("REMOVE COLUMN==>",sql)
                            with connection.cursor() as cursor:
                                cursor.execute(sql)
                                connection.commit()
                        else:
                            for j in range(len(list_of_dev_id)):
                                if list_of_dev_id[j] == id[0]:
                                    data = dev_queries[j].split("VALUES")
                                    data = data[0].split("INSERT INTO " + '`'+  table+ '`' + ' (')[1].replace('`','').replace(')','').split(',')
                                    stri = ""
                                    for d in data:
                                        d = d.strip()
                                        stri += '`' + d + '`' + '=values(`' + d + '`),'
                                    stri = stri[:-1]
                                    sql = dev_queries[j] + ") ON DUPLICATE KEY UPDATE " + stri
                                    print("UPDATE==>", sql)
                                    if sql.endswith(')') or sql.endswith(');') or sql.endswith(') ;'):
                                        sql = sql
                                    else:
                                        sql += ')'
                                    with connection.cursor() as cursor:
                                        cursor.execute(sql)
                                        connection.commit()

            if len(dev_queries) == len(prod_queries):
                for i in range(len(dev_queries)):
                    dev_qury = dev_queries[i].split('`) VALUES (')
                    dev_data = dev_qury[1]
                    prod_qury = prod_queries[i].split('`) VALUES (')
                    prod_data = prod_qury[1]
                    if (dev_query == prod_query and dev_data != prod_data) or dev_data != prod_data:
                        if dev_queries[i] not in prod_queries:
                            values = dev_queries[i].split("VALUES (")
                            id = values[1].split(',')
                            if id[0] not in list_of_prod_id:
                                sql = dev_queries[i]
                                if sql.endswith(')') or sql.endswith(');') or sql.endswith(') ;'):
                                    sql = sql
                                else:
                                    sql += ')'
                                with connection.cursor() as cursor:
                                    cursor.execute(sql)
                                    connection.commit()
                            else:
                                #"id is in prod"
                                for j in range(len(list_of_dev_id)):
                                    if list_of_dev_id[j] == id[0]:
                                        data = dev_queries[j].split("VALUES")
                                        data = data[0].split("INSERT INTO " + '`' + table + '`' + ' (')[1].replace('`','').replace(')', '').split(',')
                                        stri = ""
                                        for d in data:
                                            d = d.strip()
                                            stri += '`' + d + '`'+'=values(`' + d + '`),'
                                        stri = stri[:-1]
                                        sql = dev_queries[j] + ") ON DUPLICATE KEY UPDATE " + stri
                                        print("UPDATE==>", sql)
                                        if sql.endswith(')') or sql.endswith(');') or sql.endswith(') ;'):
                                            sql = sql
                                        else:
                                            sql += ')'
                                        with connection.cursor() as cursor:
                                            cursor.execute(sql)
                                            connection.commit()

                        if prod_queries[i] not in dev_queries:
                            values = prod_queries[i].split("VALUES (")
                            id = values[1].split(',')
                            values = prod_queries[i].split("VALUES (")
                            if id[0] not in list_of_dev_id:
                                id = values[1].split(',')
                                name = values[0].split('(`')
                                column_name = name[1].split('`,')
                                sql = "DELETE from " + table + " where " + column_name[0] + ' = ' + id[0]
                                print("COMPARE COLUMN to PROD==>", sql)
                                with connection.cursor() as cursor:
                                    cursor.execute(sql)
                                    connection.commit()
    except Exception as e:
        print("IN QUERY COMPARE==>",e)
        revert_back_database(schema_name=qa_database, username=qa_user, password=qa_password, file_path=qa_file_path)

#YOU HAVE TO CHANGE THE LOGIC LIKE "DEV TO QA" OR "QA TO PROD"
#IN THIS METHOD AS PER REQUIRMENT
def exception_table():
    try:
        #for Intent Details:
        with connection.cursor() as cursor:
            sql = "Select * from intent_details"
            cursor.execute(sql)
            for data in cursor:
                if (Environment_Migration =="LOCAL TO DEV") and \
                        ("QA" in str(data['agent_name']) or "PROD" in str(data['agent_name'])):
                        agent_name = str(data['agent_name']).replace("QA","DEV").replace("PROD","DEV")
                        sql = "UPDATE `intent_details` SET `agent_name`=" + "'" + agent_name + "'" + " where `intent_id`=" + "'" + str(data['intent_id']) + "'" + ';'
                        print("data==>", sql)
                        with connection.cursor() as cursor:
                            cursor.execute(sql)
                            connection.commit()
                if (Environment_Migration =="DEV TO QA") and \
                        ("DEV" in str(data['agent_name']) or "PROD" in str(data['agent_name'])):
                        agent_name = str(data['agent_name']).replace("DEV","QA").replace("PROD","QA")
                        sql = "UPDATE `intent_details` SET `agent_name`=" + "'" + agent_name + "'" + " where `intent_id`=" + "'" + str(data['intent_id']) + "'" + ';'
                        print("data==>", sql)
                        with connection.cursor() as cursor:
                            cursor.execute(sql)
                            connection.commit()
                if (Environment_Migration =="QA TO PROD") and \
                        ("QA" in str(data['agent_name']) or "DEV" in str(data['agent_name'])):
                        agent_name = str(data['agent_name']).replace("QA","PROD").replace("DEV","PROD")
                        sql = "UPDATE `intent_details` SET `agent_name`=" + "'" + agent_name + "'" + " where `intent_id`=" + "'" + str(data['intent_id']) + "'" + ';'
                        print("data==>", sql)
                        with connection.cursor() as cursor:
                            cursor.execute(sql)
                            connection.commit()
    except Exception as e:
        print(e)
        revert_back_database(schema_name=qa_database, username=qa_user, password=qa_password, file_path=qa_file_path)


def revert_back_database(schema_name,username,password,file_path):
    try:
        sql = "DROP SCHEMA " + schema_name
        with connection.cursor() as cursor:
            cursor.execute(sql)
            connection.commit()
        sql = "CREATE SCHEMA " + schema_name
        with connection.cursor() as cursor:
            cursor.execute(sql)
            connection.commit()

        database_files = os.listdir(FOLDER_PATH)
        for f_name in database_files:
            shutil.copy2(FOLDER_PATH + f_name,FOLDER_PATH + f_name.replace('.txt','.SQL'))

        print('mysql ' + schema_name + ' -u' + username +' -p' + password + ' < ' + file_path)
        os.system('mysql ' + schema_name + ' -u' + username + ' -p' + password + ' < ' + file_path)
    except Exception as e:
        print(e)
        send_mail_if_restore_was_failed(e)

"""   IF Something Error was Found while Auto Deployment of Database   """

def send_mail_if_restore_was_failed(error_message):
        try:
            sender_email = "test1.p@turabit.com"
            password = "Tura@Sup$9900"
            receiver_email = "vidit.s@turabit.com"
            message = MIMEMultipart("alternative")
            message["Subject"] = "Auto Deployment Failed"
            message["From"] = sender_email
            message["To"] = receiver_email
            html = """
                        <html>
                          <body>
                            Hello Production Team, <b>Auto Deployment of DataBase was Failed</b> Due to Some Error Please Go and Check Manually there. 
                               <br><br><b>ERROR: </b>""" \
                            + str(error_message) +"""
                           <br><br>Thank You,<br>
                           Turabit Support.
                          </body>
                        </html>
                    """
            part2 = MIMEText(html, "html")
            message.attach(part2)
            context = ssl.create_default_context()
            with smtplib.SMTP_SSL("premium32.web-hosting.com", 465, context=context) as server:
                server.login(sender_email, password)
                server.sendmail(
                    sender_email, receiver_email, message.as_string()
                )
                print("ERROR Mail Sent Successfully.......")
                exit()
        except Exception as e:
            print(e)
            exit()

""" FOR DIALOGFLOW RELATED CONFIG FILE """
def dialogflow_config_file():
    try:
        credentials = []
        for data in range(len(env)):
            agent_id = []
            agent_json = []
            host = configurations[env[data]]["host"]
            user = configurations[env[data]]["user"]
            password = configurations[env[data]]["password"]
            database = configurations[env[data]]["database"]
            connection = pymysql.connect(host=host,
                                 user=user,
                                 password=password,
                                 db=database,
                                 cursorclass=pymysql.cursors.DictCursor)
            with connection.cursor() as cursor:
                sql = "select * from df_agents;"
                cursor.execute(sql)
                for data in cursor:
                    agent_id.append(data["df_agent_projectid"])
                    agent_json.append(data["google_service_key"].split('/')[1])
                print("Dialog_flow_sql==>", sql)
            credentials.append(agent_id)
            credentials.append(agent_json)
        #total_length = len(credentials[0])
        for ln in range(len(credentials[0])):
            dev_agent_id = credentials[0][ln]
            dev_json = credentials[1][ln]
            qa_agent_id = credentials[2][ln]
            qa_json = credentials[3][ln]
            data =  {
                        "files": os.getcwd() + "\Dialogflow\\" + num_to_words.number_to_words(ln),
                        "dev_file_path" : os.getcwd() + "\Dialogflow\\" + num_to_words.number_to_words(ln) +"\DEV",
                        "dev_extract_folder" : "\d_dev",
                        "dev_extract_files" : os.getcwd() + "\Dialogflow\\" +  num_to_words.number_to_words(ln) +"\DEV\d_dev",
                        "source_entities" : os.getcwd() + "\Dialogflow\\" + num_to_words.number_to_words(ln) + "\DEV\d_dev\entities\\",
                        "source_intents" : os.getcwd() + "\Dialogflow\\"+  num_to_words.number_to_words(ln) + "\DEV\d_dev\\intents\\",
                        "Dev_projectID" : dev_agent_id,
                        "Dev_credentials" : os.getcwd() + "\JSON\\" + dev_json,

                        "qa_file_path" :  os.getcwd() + "\Dialogflow\\"+ num_to_words.number_to_words(ln) +"\QA",
                        "qa_extract_folder" : "\q_qa",
                        "qa_extract_files" :  os.getcwd() + "\Dialogflow\\"+ num_to_words.number_to_words(ln) +"\QA\q_qa",
                        "destination_entities" :  os.getcwd() + "\Dialogflow\\"+ num_to_words.number_to_words(ln) +"\QA\q_qa\entities\\",
                        "destination_intents" :  os.getcwd() + "\Dialogflow\\"+ num_to_words.number_to_words(ln) +"\QA\q_qa\intents\\",
                        "Qa_projectID": qa_agent_id,
                        "Qa_credentials" : os.getcwd() + "\JSON\\" + qa_json,

                        "new_created_zip": os.getcwd() + "\Dialogflow\\"+ num_to_words.number_to_words(ln) +"\created_zip",
                        "created_zip_file_name": "created_zip_file"
                    }
            dialogflow_config = open(os.getcwd() + "\\dialogflow_config.txt",'a')
            dialogflow_config.write(str(data).replace("'",'"') + "," )
            dialogflow_config.close()
        dialogflow_config = open(os.getcwd() + "\\dialogflow_config.txt",'r+')
        dialog_flow_data = dialogflow_config.read()

        dialog_flow_data = "[" + dialog_flow_data[:-1] + "]"
        dialogflow_config.truncate(0)
        dialogflow_config.write(dialog_flow_data)
        dialogflow_config.close()
    except Exception as e:
        print(e)


try:
    num_to_words = inflect.engine()

    config = open(os.getcwd() + "\\db_config", "r")
    configurations = config.read()
    configurations = eval(configurations)
    Environment_Migration = configurations["Environment_Migration"]
    env = Environment_Migration.split(" TO ")

    """" For DEV Instance Credentials """

    dev_host = configurations[env[0]]["host"]
    dev_user = configurations[env[0]]["user"]
    dev_password = configurations[env[0]]["password"]
    dev_database = configurations[env[0]]["database"]
    dev_file_path = os.getcwd() + configurations[env[0]]["file_path"]

    """ For QA Instance credentials """

    qa_host = configurations[env[1]]["host"]
    qa_user = configurations[env[1]]["user"]
    qa_password = configurations[env[1]]["password"]
    qa_database = configurations[env[1]]["database"]
    qa_file_path = os.getcwd() + configurations[env[1]]["file_path"]

    os.mkdir(os.getcwd() + "\\DB_FILE")
    take_dump_from_database(username=dev_user, password=dev_password, database=dev_database, file_path=dev_file_path,
                            deleted_old_files=False)
    take_dump_from_database(username=qa_user, password=qa_password, database=qa_database, file_path=qa_file_path,
                            deleted_old_files=True)

    connection = pymysql.connect(host=qa_host,
                                 user=qa_user,
                                 password=qa_password,
                                 db=qa_database,
                                 cursorclass=pymysql.cursors.DictCursor)
    dev_file = "DEV.txt"
    FOLDER_PATH = os.getcwd() + "\\DB_FILE\\"
    sql_files = os.listdir(FOLDER_PATH)
    files = []

    for i in sql_files:
        p = Path(FOLDER_PATH + i)
        p.rename(p.with_suffix('.txt'))

    txt_files = os.listdir(FOLDER_PATH)
    for j in txt_files:
        f = open(FOLDER_PATH + j, "r", encoding="utf-8")
        lines = f.readlines()
        endofcreate = False
        create_statements = []
        create_statement = ""
        for line in lines:
            if line.__contains__('CREATE TABLE') or endofcreate:
                endofcreate = True
                create_statement = create_statement + " " + line
            if line.__contains__('ENGINE='):
                create_statements.append(create_statement)
                create_statement = ""
                endofcreate = False

        files.append(create_statements)

    table_length_of_files = False
    if len(files[0]) == len(files[1]): table_length_of_files = True
    table_list = []

    compare_structure(table_length_of_files, files)
    create_table(table_length_of_files, files)
    query_data_compare()
    exception_table()
    dialogflow_config_file()
except Exception as e:
    print(e)
    send_mail_if_restore_was_failed(e)