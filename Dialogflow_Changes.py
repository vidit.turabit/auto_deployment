import os
from pathlib import Path
from zipfile import ZipFile
import shutil
import dialogflow_v2beta1
import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def dialogflow_export_agent(credentials,projectid,file_path):
    try:
        os.mkdir(file_path)
        os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = credentials
        qa_client = dialogflow_v2beta1.AgentsClient()
        qa_parent = qa_client.project_path(projectid)
        response = qa_client.export_agent(qa_parent)
        zip_prepare(zipfilepath=file_path,result=response.result(),projectid = projectid)
    except Exception as e:
        print(e)
        send_mail_if_restore_was_failed(e)

def zip_prepare( zipfilepath, result,projectid):
    try:
        zip_files  = os.listdir(zipfilepath)
        for i in range(len(zip_files)):
            try:
                shutil.rmtree(zipfilepath +'\\'+ zip_files[i] )
            except:
                os.remove(zipfilepath +'\\'+ zip_files[i])
        f = open(zipfilepath+"\\"+projectid+".zip", 'wb')
        f.write(result.SerializeToString())
        f.close()

    except Exception as e:
        print(e)
        send_mail_if_restore_was_failed(e)

def extract_files_from_zip(file_path,extract_folder):
    try:
        if os.path.exists(extract_folder):
            shutil.rmtree(extract_folder)
        files = os.listdir(file_path)
        for file in files:
            if file.endswith('.zip'):
                filepath = file_path + '\\' + file
                with ZipFile(filepath, 'r') as zip:
                    zip.extractall(extract_folder)
                    return zip.namelist()
    except Exception as e:
        print(e)
        send_mail_if_restore_was_failed(e)

def json_files_list(file_path):
    try:
        json_files = []

        files = os.listdir(file_path + '\\' + 'entities')
        for file in files:
            p = Path(file_path + '\\' + 'entities' + '\\' + file)
            p.rename(p.with_suffix('.txt'))
        files = os.listdir(file_path + '\\' + 'entities')
        for file in files:
            if file.endswith('.txt') and not file.startswith('test'):
                filepath = file_path + '\\' + 'entities' + '\\' + file
                json_files.append(filepath)
        dict_files = {'entities':json_files}
        json_files = []
        files = os.listdir(file_path + '\\' + 'intents')
        for file in files:
            p = Path(file_path + '\\' + 'intents' + '\\' + file)
            p.rename(p.with_suffix('.txt'))
        files = os.listdir(file_path  + '\\' + 'intents')
        for file in files:
            if file.endswith('.txt') and not file.startswith('test'):
                filepath = file_path + '\\' + 'intents' + '\\' + file
                json_files.append(filepath)
        dict_files['intents'] = json_files
        return dict_files
    except Exception as e:
        print(e)
        send_mail_if_restore_was_failed(e)


def compair_entities_json_files(dev_dict,qa_dict,destination_entities,source_entities):
    try:
        dev_entities_files= []
        qa_entities_files = []
        new_entities = []
        dev_entities = dev_dict['entities']
        qa_entities = qa_dict['entities']

        #DEV entities files
        for i in range(len(dev_entities)):
            dev_file = dev_entities[i].split('\\')
            dev_entities_files.append(dev_file[-1])
        #QA entities files
        for i in range(len(qa_entities)):
            qa_file = qa_entities[i].split('\\')
            qa_entities_files.append(qa_file[-1])

        #IF NEW Entities in DEV which was not Present in QA
        if len(dev_entities_files) > len(qa_entities_files):
            for i in range(len(dev_entities_files)):
                if dev_entities_files[i] not in qa_entities_files:
                    for j in range(len(dev_entities)):
                        if (dev_entities_files[i] in str(dev_entities[j])) and dev_entities[j] not in new_entities:
                            new_entities.append(dev_entities[j])

        #If Some Deleted Entries in DEV Which all are still Present in QA
        if len(dev_entities_files) < len(qa_entities_files):
            for i in range(len(qa_entities_files)):
                if qa_entities_files[i] not in dev_entities_files:
                    os.remove(destination_entities + qa_entities_files[i])
                    qa_entities.remove(destination_entities + qa_entities_files[i])
        for i in range(len(new_entities)): dev_entities.remove(new_entities[i])

        #Recheck in BOTH Folder DEV As well As QA for Entities files
        check_dev_entitis = os.listdir(source_entities)
        check_qa_entities = os.listdir(destination_entities)
        for i in range(len(check_dev_entitis)):
            if check_dev_entitis[i] not in check_qa_entities:
                for j in range(len(dev_entities)):
                    if (dev_entities_files[i] in str(dev_entities[j])) and dev_entities[j] not in new_entities:
                        new_entities.append(dev_entities[j])

        #remove those entities which all are just created in QA via DEV because in that we dont need to compare.
        if len(new_entities) > 0:
            for i in range(len(new_entities)):
                print(new_entities[i])
                dev_entities.remove(new_entities[i])
                new_added_file = new_entities[i].split('\\')
                dev_file = open(new_entities[i], 'r',encoding='utf8')
                file = open(destination_entities + new_added_file[-1], 'w',encoding='utf8')
                file.write(dev_file.read())
                file.close()
        for i in range(len(dev_entities)):
            for i in range(len(dev_entities)):
                dev_entitie = dev_entities[i]
                qa_entitie = qa_entities[i]
                dev_file = open(dev_entitie , 'r',encoding='utf8')
                qa_file = open(qa_entitie , 'r',encoding='utf8')
                dev_file = dev_file.read()
                qa_file = qa_file.read()
                if dev_file != qa_file:
                    print(qa_entities[i])
                    file = open(qa_entities[i] , 'w',encoding='utf8')
                    file.write(dev_file)
                    file.close()
    except Exception as e:
        print(e)
        send_mail_if_restore_was_failed(e)

def compair_intent_json_files(dev_dict,qa_dict,destination_intents,source_intents):
    try:
        dev_intent_files= []
        qa_intent_files = []
        new_intents = []
        dev_intents = dev_dict['intents']
        qa_intents = qa_dict['intents']

        #DEV intent files
        for i in range(len(dev_intents)):
            dev_file = dev_intents[i].split('\\')
            dev_intent_files.append(dev_file[-1])

        #QA intent files
        for i in range(len(qa_intents)):
            qa_file = qa_intents[i].split('\\')
            qa_intent_files.append(qa_file[-1])

        #IF NEW intents in DEV which was not Present in QA
        if len(dev_intent_files) > len(qa_intent_files):
            for i in range(len(dev_intent_files)):
                if dev_intent_files[i] not in qa_intent_files:
                    for j in range(len(dev_intents)):
                        if (dev_intent_files[i] in str(dev_intents[j])) and dev_intents[j] not in new_intents:
                                new_intents.append(dev_intents[j])

        #If Some Deleted intents in DEV Which all are still Present in QA
        if len(dev_intent_files) < len(qa_intent_files):
            for i in range(len(qa_intent_files)):
                if qa_intent_files[i] not in dev_intent_files:
                    os.remove(destination_intents + qa_intent_files[i])
                    qa_intents.remove(destination_intents + qa_intent_files[i])

        #Recheck in BOTH Folder DEV As well As QA for intent files
        check_dev_intents = os.listdir(source_intents)
        check_qa_intents = os.listdir(destination_intents)
        if len(check_dev_intents) > len(check_qa_intents):
            for i in range(len(check_dev_intents)):
                if check_dev_intents[i] not in check_qa_intents:
                    for j in range(len(dev_intents)):
                        if (dev_intent_files[i] in str(dev_intents[j])) and dev_intents[j] not in new_intents:
                                new_intents.append(dev_intents[j])

        #remove those intents which all are just created in QA via DEV because in that we don't need to compare.
        if len(new_intents) > 0:
            for i in range(len(new_intents)):
                print(new_intents[i])
                dev_intents.remove(new_intents[i])
                new_added_file = new_intents[i].split('\\')
                dev_file = open(new_intents[i], 'r',encoding='utf8')
                file = open(destination_intents + new_added_file[-1], 'w',encoding='utf8')
                file.write(dev_file.read())
                file.close()
        for i in range(len(dev_intents)):
            for i in range(len(dev_intents)):
                dev_intent = dev_intents[i]
                qa_intent = qa_intents[i]
                dev_file = open(dev_intent , 'r',encoding='utf8')
                qa_file = open(qa_intent , 'r',encoding='utf8')
                dev_file = dev_file.read()
                qa_file = qa_file.read()
                if dev_file != qa_file:
                    print(qa_intents[i])
                    file = open(qa_intents[i] , 'w',encoding='utf8')
                    file.write(dev_file)
                    file.close()
    except Exception as e:
        print(e)
        send_mail_if_restore_was_failed(e)

def convert_txt_to_json_and_zip(file_path,dev_extract_files,new_created_zip,created_zip_file_name):
    try:
        files = os.listdir(file_path + '\\' + 'entities')
        for file in files:
            p = Path(file_path + '\\' + 'entities' + '\\' + file)
            p.rename(p.with_suffix('.json'))
        files = os.listdir(file_path + '\\' + 'intents')
        for file in files:
            p = Path(file_path + '\\' + 'intents' + '\\' + file)
            p.rename(p.with_suffix('.json'))
        shutil.rmtree(dev_extract_files)
        shutil.make_archive(
            new_created_zip + "\\" + created_zip_file_name,
            'zip',  # the archive format - or tar, bztar, gztar
            root_dir=file_path,  # root for archive - current working dir if None
            base_dir=None)
    except Exception as e:
        print(e)
        send_mail_if_restore_was_failed(e)

def dialogflow_restore_agent(credentials,projectid,new_created_zip,created_zip_file_name):
    try:
        os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = credentials
        qa_client = dialogflow_v2beta1.AgentsClient()
        qa_parent = qa_client.project_path(projectid)
        f = open(new_created_zip + "\\" + created_zip_file_name + ".zip",'rb')
        data = f.read()
        f.close()
        qa_client.restore_agent(parent=qa_parent, agent_content=data)
    except Exception as e:
        print(e)
        send_mail_if_restore_was_failed(e)

def start_auto_deployment():
    try:
        for agent in agent_files:
            os.makedirs(agent["files"])
            try:
                dialogflow_export_agent(agent["Dev_credentials"], agent["Dev_projectID"], agent["dev_file_path"])
                dialogflow_export_agent(agent["Qa_credentials"], agent["Qa_projectID"], agent["qa_file_path"])
            except Exception as e:
                print(e)
                send_mail_if_restore_was_failed(e)
            extract_files_from_zip(agent["dev_file_path"], agent["dev_extract_files"])
            extract_files_from_zip(agent["qa_file_path"], agent["qa_extract_files"])

            if os.path.exists(agent["source_entities"]) and os.path.exists(agent["destination_entities"]):
                dev_files = json_files_list(agent["dev_extract_files"])
                qa_files = json_files_list(agent["qa_extract_files"])
                compair_entities_json_files(dev_files, qa_files, agent["destination_entities"],agent["source_entities"])

            if os.path.exists(agent["source_intents"]) and os.path.exists(agent["destination_intents"]):
                dev_files = json_files_list(agent["dev_extract_files"])
                qa_files = json_files_list(agent["qa_extract_files"])
                compair_intent_json_files(dev_files, qa_files, agent["destination_intents"],agent["source_intents"])
                convert_txt_to_json_and_zip(agent["qa_extract_files"],agent["dev_extract_files"],agent["new_created_zip"],agent["created_zip_file_name"])
                dialogflow_restore_agent(agent["Qa_credentials"], agent["Qa_projectID"],agent["new_created_zip"],agent["created_zip_file_name"])

        shutil.rmtree(delete_folder_after_successful_deploy)
    except Exception as e:
        print(e)
        for agent in agent_files:
            restore_old_agent(agent["Qa_credentials"], agent["Qa_projectID"],agent["qa_file_path"])

def restore_old_agent(credentials,projectid,qa_file_path):
    try:
        old_agent = ""
        is_path = os.path.exists(qa_file_path)
        if is_path:
            dir = os.listdir(qa_file_path)
            for d in range(len(dir)):
                if ".zip" in d:
                    old_agent = dir[d]
            os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = credentials
            qa_client = dialogflow_v2beta1.AgentsClient()
            qa_parent = qa_client.project_path(projectid)
            f = open(qa_file_path + "\\" + old_agent,'rb')
            data = f.read()
            f.close()
            qa_client.restore_agent(parent=qa_parent, agent_content=data)
    except Exception as e:
        print(e)
        send_mail_if_restore_was_failed(e)


"""   IF Something Error was Found while Auto Deployment of Agent in DialogFlow   """
def send_mail_if_restore_was_failed(error_message):
    try:
        sender_email = "test1.p@turabit.com"
        password = "Tspl@123$"
        receiver_email = "vidit.s@turabit.com"
        message = MIMEMultipart("alternative")
        message["Subject"] = "Auto Deployment Failed"
        message["From"] = sender_email
        message["To"] = receiver_email
        html = """
                    <html>
                      <body>
                       Hello Production Team, <b>Auto Deployment of DialogFlow Agent was Failed</b> Due to Some Error Please Go and Check Manually there. 
                           <br><br><b>ERROR: </b>""" \
                            + str(error_message) +"""
                           <br><br>Thank You,<br>
                           Turabit Support.
                      </body>
                    </html>
                """

        part2 = MIMEText(html, "html")
        message.attach(part2)
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL("premium32.web-hosting.com", 465, context=context) as server:
            server.login(sender_email, password)
            server.sendmail(
                sender_email, receiver_email, message.as_string()
            )
            print("ERROR Mail Sent Successfully.......")
            exit()
    except Exception as e:
        print(e)
        exit()

try:
    delete_folder_after_successful_deploy = os.getcwd() +"\Dialogflow"
    f_file = open("dialogflow_config.txt","r",encoding="utf8")
    agent_files = f_file.read().replace(chr(0), '')
    agent_files = eval(agent_files)
    start_auto_deployment()
except Exception as e:
    print(e)
    send_mail_if_restore_was_failed(e)